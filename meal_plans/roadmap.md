## requirements road map 

continuous of scruptious project 

Application needs 
- Recipe Model 
- Template inheritance 
- Login set up and working 
- At least one recipe in database 
and at least two users in  database


Mean plans are personal. Users can create special collection of recipes and not see anyone else's

login with either two users and check that they can't see each other's meal plans 

MealPlan model 
Code location: own Django app named meal_plans, not in recipes or tags. 

1. Create Djzango app called meal plans 
2. Create MealPlan model inside the app. 
    - 4 pieces of data 
        - name, date. owner, recipes
3. Add fields to model. 
-       Refer to Recipe model 
5. Create our paths to see details of specific meal plan. List of mp, create, details. edit, and delete
6. Create Views:- Create ListView 
                - only show meal plans created by user using Authorization. 
                - link to create view
                - name of each plan links to detail view
7. CreateView 
                - show form that allows the user to enter a name, date, and slelect the recipes they want in plan 
                - When saves, user object should be automatically saved to the owner proprty of the meal plan 
                - User should be redirected to the detail page for the newly-created meal plan. 
                - Extra piece of code to make sure meals are saved with the meal plan. 
8. DetailView
                - Name, date it will be served, and list of names of the recipes associated with it. 
                - Each recipe name links to the recipe page
                - should have link to edit view 
                - link to delete view 
                - If person is not owner, then they shouldn't see meal plan 
9. EditView 
                - show form that allows owner to change it \
                -  If person is not owner, then they shouldn't see meal plan 
10. DeleteView 
                -Show form that allows person who owns meal plan to delete it 
                - After succesful deletion, user should be directed back to list view for meal plans. 
                -if the person accessing the page is not te owner of the meal plan, then they should not see the meal plan. 


### Templates 
- All Django html templates sould extend base.html file
- Alter base to show "Login" link in nav bar when current user is not autenticated. Can use default Django login. 
- When creating users, create regular users and not super users. 
- Can just make them superusers. 

- add to scrumptious recipes hardening as a seperate app. 

How do I know where each app starts/ends and how do I add to main app/project. 
                