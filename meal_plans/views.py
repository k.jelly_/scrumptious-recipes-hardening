from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.views.generic.detail import DetailView
from django.shortcuts import redirect

from meal_plans.models import MealPlan


# Create your views here.


class MealPlanListView(LoginRequiredMixin, ListView):

    model = MealPlan
    template_name = "meal_plans/list.html"
    # paginate_by = 2
    context_object_name = "mealplan"
    # can use function instead to get context data

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
        # returns list of objects associated with model. And associates the owner attribute in mealplan object to the user who sent the request


class MealPlanCreateView(CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "recipes", "date"]
    context_object_name = "mealplan"
    # success_url = reverse_lazy("mealplan_list")

    def form_valid(self, form):
        plan = form.save(commit=False)
        # plan is saved form object
        plan.owner = self.request.user
        # assign owner of plan to user who sent request.

        # save the plan
        form.save()
        form.save_m2m()

        # maybe saves many mealPlan objects to one user and one user to many mealPlans.

        return redirect("mealplans/detail.html", pk=plan.id)

    def get_success_url(self):
        return reverse_lazy("mealplans/detail.html", args=[self.object.id])


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "recipes", "date"]
    context_object_name = "mealplan"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("mealplans/detail.html", args=[self.object.id])


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"
    context_object_name = "mealplan"

    # should display list of each recipe in meal plan

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


# class MealPlanDetailView()
