from django.urls import path

# from django.contrib.auth import views as auth_views


from meal_plans.views import (
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanDetailView,
    MealPlanListView,
    MealPlanUpdateView,
)


urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plans_list"),
    path(
        "<int:pk>/detail/",
        MealPlanDetailView.as_view(),
        name="mealplans/detail.html",
    ),
    path("create/", MealPlanCreateView.as_view(), name="mealplans_new"),
    path(
        "<int:pk>/edit/",
        MealPlanUpdateView.as_view(),
        name="mealplans_edit",
    ),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="mealplans_delete",
    ),
]
