# from datetime import date, datetime
from django.db import models
from django.conf import settings


USER_MODEL = settings.AUTH_USER_MODEL
# stores the authorized user in the User_model object


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField(auto_now_add=False, null=True)
    owner = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="recipes")
    # once recipe can be apart of several meal plans


# related name must be name of app


# Create your models here.
