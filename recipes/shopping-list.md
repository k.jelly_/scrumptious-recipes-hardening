




2. Create ShoppingItem model inside Recipes app.
    User: 
        type: Auth_User_Model 

    food_item: models.Foreignkey(user model, cascade on delete)
            Data type: foreign key to FoodItem, protect on delete
            One user can have many shopping items. one-to-many relationship. Foreign key from shopping item to user model. 
            One shopping item can have one user.
            Refer to your Recipe model to see how to create a foreign key to the AUTH_USER_MODEL.

            One shopping list can have one food item. One food item can be on many different users' shopping lists. Foreign key from shopping list to food item. 
            Refer to your Ingredient model to see how to create a foreign key relationship to FoodItem.


            Create a function view that handles a POST method that creates a shopping item instance in the database based on the submitted ingredient id in the form

            Add a custom HTML form to the ingredients table on the recipes detail page with its action pointing to the URL of the view you registered in the previous step

3. urls.py, settings.py, and urls.py

Register post function view in the recipes Django app's urls.py
            1. Add a shopping item to the user's shopping list
                shopping_items/create/
            2. Show the list of shopping items on the user's shopping list
                shopping_items/
            3. Remove all shopping items from a user's shopping list
                shopping_items/delete/

    
4. Views 
    
    1. List View: 
          recipes/shopping_items/
         -  shows a link to shopping list in nav bar, and the number of items in the basket. 
          - button on ths page that deletes all items from user's shopping list
    
    2. Create View: 
            recipes/shopping_items/create/
            handles ONLY HTTP POST requests 
            - NO HTML TEMPLATE 
            - creates a ShoppingItem instance in the database. 
             based on the current user and the value of the submitted "ingredients_id" value.
            
          Reverse_laz() After the successful creation of the shopping item, the browser should be redirected back to the recipe page that it came from.
    
    3. Delete view: 
            handles only HTTP POST requests, which means there's no HTML template for it.
             It should delete ShoppingItem instances associated with the current user. Then, it should show the empty shopping list after the deletion





  <td>
          <form method="POST" action="{% url 'shopping_item_create' %}">
            {% csrf_token %}
            <input type="hidden" name="ingredient_id" value="{{ ingredient.id }}">
            <button>+ shopping list</button>
          </form>
    </td>


    
        {% if ingredient.fooditem not in shoppingitems.all.fooditem %}

      <tr>
        <!-- Add this new code -->
        <td>
          <form method="POST" action="{% url 'shopping_item_create' %}">
            {% csrf_token %}
            <input type="hidden" name="ingredient_id" value="{{ ingredient.id }}">
            <button>+ shopping list</button>
          </form>
        </td>

        {% else %}
        <td>
          <div> in your list</div>
        </td>

      </tr>
      {% endif %}
